###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

all: main

main.o:  main.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

node.o: node.hpp
	$(CXX) -c node.cpp

list.o: list.cpp node.hpp list.hpp
	$(CXX) -c list.cpp

main: *.hpp main.o cat.o node.o list.o
	$(CXX) -o $@ main.o cat.o node.o list.o

queueSim.o: queueSim.cpp *.hpp
	$(CXX) -c $(CXXFLAGS) $<

queueSim: *.hpp queueSim.o list.o node.o
	$(CXX) -o $@ queueSim.o list.o node.o

testfunc.o: testfunc.cpp node.hpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

testfunc: node.hpp list.hpp testfunc.o node.o list.o
	$(CXX) -o $@ testfunc.o node.o list.o

test.o: test.cpp *.hpp
	$(CXX) -c $(CXXFLAGS) $<

test: *.hpp test.o cat.o list.o node.o
	$(CXX) -o $@ test.o cat.o list.o node.o

clean:
	rm -f *.o main queueSim testfunc test
