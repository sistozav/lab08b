///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab @Todo
//
//
// Usage: @Todo
//
//
// @author Vinton Sistoza <sistozav@wiliki>
// @date   4/1/21
///////////////////////////////////////////////////////////////////////////////
#include <cassert>
#include <iostream>
#include "list.hpp"

using namespace std;
const bool DoubleLinkedList::empty() const{
   Node* tempNode = head;
   if(tempNode == nullptr)
      return true;
   else
      return false;
  };
void DoubleLinkedList::push_front(Node* newNode){
   if(newNode == nullptr) //If newNode is nothing
      return;
   if(head != nullptr){ //Check if Empty
      newNode->prev = nullptr;
      newNode->next = head;
      head->prev = newNode;
      head = newNode;
   }
   else{
      newNode->next = nullptr;
      newNode->prev = nullptr;
      head = newNode;
      tail = newNode;
   }
   count++;
};
Node* DoubleLinkedList::pop_front(){
   Node* tempNode = head;
   if(tempNode == nullptr)
      return nullptr;
   else{
      head = head->next;
      head->prev = nullptr;
      count--;
      return tempNode;
   }

};
Node* DoubleLinkedList::get_first() const{
   Node* tempNode = head;
   return tempNode;
};

Node* DoubleLinkedList::get_next(const Node* currentNode) const{
   Node* tempNode;
   tempNode = currentNode->next;
   return tempNode;
};
void DoubleLinkedList::push_back(Node* newNode){
   Node* tempNode = tail;
   if(tail == nullptr)
      return;
   else{
      newNode->prev = tempNode;
      tempNode->next = newNode;
      newNode->next = nullptr;
      tail = newNode;
   }
};
Node* DoubleLinkedList::pop_back(){
   Node* tempNode = tail;
   if(tail == nullptr)
      return nullptr;
   else{
      tail = tail->prev;
      tail->next = nullptr;
      count--;
      return tempNode;
   }
};
Node* DoubleLinkedList::get_last() const{
   Node* tempNode = tail;
   return tempNode;
};
Node* DoubleLinkedList::get_prev(const Node* currentNode) const{
   Node* tempNode;
   if(currentNode == nullptr)
      return nullptr;
   else{
      tempNode = currentNode->prev;
      return tempNode;
   }
};
void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode){
   Node* tempNode = currentNode;
   if(currentNode == nullptr && head == nullptr){
      push_front(newNode);
      return;
   }
   if(currentNode != nullptr && head == nullptr)
      assert(false);
   if(currentNode == nullptr && head != nullptr)
      assert(false);
   assert(currentNode != nullptr && head != nullptr);
   if(currentNode != nullptr && head != nullptr){
      newNode->prev = currentNode;
      newNode->next = currentNode->next;
      tempNode = currentNode->next;
      tempNode->prev = newNode;
      currentNode->next = newNode;
      count++;
   }
};
void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode){
   Node* tempNode = currentNode;
   if(currentNode == nullptr){
      push_front(newNode);
      count++;
   }
   else{
      tempNode = tempNode->prev;
      tempNode->next = newNode;
      newNode->next = currentNode;
      newNode->prev = tempNode;
      currentNode->prev = newNode;
      count++;
   }
};
const bool DoubleLinkedList::isIn(Node* aNode) const{
   Node* currentNode = head;
   while(currentNode != nullptr){
      if(aNode == currentNode)
         return true;
      currentNode = currentNode->next;
   }
   return false;
};
bool DoubleLinkedList::validate() const{
   if(head == nullptr){
      assert(tail == nullptr);
      //assert(count == 0);
   } else{
      assert(tail != nullptr);
      //assert(count != 0);
   }
   if(tail == nullptr){
      assert(head == nullptr);
      //assert(count == 0);
   } else{
      assert(head != nullptr);
      //assert(count != 0);
   }
   if(head != nullptr && tail == head){
      //assert(count == 1);
   }
   unsigned int forwardCount = 0;
   Node* currentNode =  head;
   while(currentNode != nullptr){
      forwardCount++;
      if(currentNode->next != nullptr){
         assert(currentNode->next->prev == currentNode);
      }
      currentNode = currentNode->next;
   }
   //assert(count == forwardCount);

   unsigned int backwardCount = 0;
   currentNode = tail;
   while(currentNode != nullptr){
      backwardCount++;
      if(currentNode->prev != nullptr){
         assert(currentNode->prev->next == currentNode);
      }
      currentNode = currentNode->prev;
   }
   //assert(count == backwardCount);
   cout << "List is valid" << endl;
   return true;
};
void DoubleLinkedList::swap(Node* node1, Node* node2){
   Node* tempNodeprev1 = node1->prev;
   Node* tempNodenext2 = node2->next;
   Node* tempNodenext1 = node1->next;
   Node* tempNodeprev2 = node2->prev;
   if(node1 == nullptr || node2 == nullptr)
      return;

   if(node1->next == node2 && node1 != head && node2 != tail){
      node2->prev = tempNodeprev1;
      node2->next = node1;
      tempNodeprev1->next = node2;
      node1->prev = node2;
      node1->next = tempNodenext2;
      tempNodenext2->prev = node1;
      //cout << "RUNNING1" << endl;
   }
   /*else if(node1->prev == node2 && node1 != tail && node2 != head){
      node2->prev = node1;
      node2->next = tempNode1;
      node1->prev = tempNodeprev2;
      node1->next = node2;
   }*/
   else if(node1 == head && node2 != tail && node1->next == node2){
      node2->prev = nullptr;
      node2->next = node1;
      head = node2;
      node1->prev = node2;
      node1->next = tempNodenext2;
      tempNodenext2->prev = node1;
      //cout << "RUNNING2" << endl;
   }
   else if(node1 == head && node2 != tail && node1->next != node2){
      node2->prev = nullptr;
      node2->next = tempNodenext1;
      tempNodenext1->prev = node2;
      head = node2;
      node1->prev = tempNodeprev2;
      node1->next = tempNodenext2;
      tempNodenext2->prev = node1;
      tempNodeprev2->next = node1;
      //cout << "RUNNING3" << endl;
   }
   else if(node1 != head && node2 == tail && node1->next != node2){
      node2->prev = tempNodeprev1;
      node2->next = tempNodenext1;
      tempNodeprev1->next = node2;
      tempNodenext1->prev = node2;
      node1->prev = tempNodeprev2;
      node1->next = nullptr;
      tail = node1;
      tempNodeprev2->next = node1;
      //cout << "RUNNING4" << endl;
   }
   else if(node1 != head && node2 == tail && node1->next == node2){
      node2->prev = tempNodeprev1;
      node2->next = node1;
      tempNodeprev1->next = node2;
      node1->prev = node2;
      node1->next = nullptr;
      tail = node1;
      //cout << "RUNNING5" << endl;
   }
   else if(node1 == head && node2 == tail && node1->next != node2){
      node2->prev = nullptr;
      node2->next = tempNodenext1;
      tempNodenext1->prev = node2;
      head = node2;
      node1->prev = tempNodeprev2;
      node1->next = nullptr;
      tempNodeprev2->next = node1;
      tail = node1;
      //cout << "RUNNING6" << endl;
   }
   else if(node1 == head && node2 == tail && node1->next == node2){
      node2->prev = nullptr;
      node2->next = node1;
      head = node2;
      node1->prev = node2;
      node1->next = nullptr;
      tail = node1;
      //cout << " RUNNNIG7" << endl;
   }
   else if(node1 == node2)
      return;
   else{
      node2->prev = tempNodeprev1;
      node2->next = tempNodenext1;
      tempNodeprev1->next = node2;
      tempNodenext1->prev = node2;
      node1->prev = tempNodeprev2;
      node1->next = tempNodenext2;
      tempNodeprev2->next = node1;
      tempNodenext2->prev = node1;
      //cout <<"RUNNING8" << endl;
   }
   
};
const bool DoubleLinkedList::isSorted() const{
   if(count <=1)
      return true;
   for(Node* i = head ; i->next != nullptr ; i->next){
      if(*i > *i->next)
         return false;
      }
   return true;
};
void DoubleLinkedList::insertionSort(){
   if(count <=1)
      return;
   for(Node* i = head ; i->next != nullptr ; i = i->next){
      Node* left = i;
      for(Node* j = i->next ; j != nullptr ; j = j->next){
         if(*left > *j)
            left = j;
      }
      swap(i, left);
      i = left;
   }

};
